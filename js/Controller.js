$(function () {

    $('#navKB').on('click', function (e) {
        selectedKB = null;
        changePage('#selectPage');
    });

    $('#navQuestion').on('click', function (e) {
        if (selectedKB !== null) {
            changePage('#questionPage');
            start();
        } else
            alert('Elija una base de conocimientos');
    });

    $('#kbNew').on('click', function (e) {
        changePage('#createPage');
        $("#show-KbCreate").show();
        $("#show-ObjCreate").hide();
        $("#show-AttrCreate").hide();
    });

    $('#addKB').on('click', function (e) {
        var kbName = document.getElementById("KBCreate").value;
        if (kbName !== "" && kbName !== null) { //se agrego que sea distinto de nulo ya que el prompt si se da cancelar devuelve null no "".
            $("#show-KbCreate").hide();
            $("#show-ObjCreate").show();
            tmpKB = new KBase(kbName);
            printKB();
        } else {
            alert("debe ingresar una base de conocimiento");
        }
        document.getElementById("KBCreate").value = "";

        return false;
    });

    $('#kbDelete').on('click', function (e) {
        setDeleteKB();
        changePage('#deletePage');
    });

    /*
     *   
     *
     *  Edit Agregado
     *
     *
     */


    $('#kbEdit').on('click', function (e) {
        setEditKB();
        changePage('#editPage');

        $("#show-ObjCreateE").show();
        $("#show-AttrCreateE").hide();
    });
    
    
    $('#kbEditUL').on('click', ".kbEditLI", function (e) {
        $.ajax({
            data: {
                "load": $(e.target).html()
            },
            url: "services.php",
            context: document.body,
            type: "post",
            success: function (response) {
                selectedKB = JSON.parse(response);
                tmpKB = selectedKB;
                changePage("#editListPage");
                editKB();

            }
        });
    });

    
    function setEditKB() {
        getKBList("#kbEditUL", "kbEditLI");
    }
    
    function editKB()
    {
        var html = "";
        var data = tmpKB;
        html = "<div style='margin-left:20px;'><h3><span class='glyphicon glyphicon-edit' aria-hidden='true' style='margin-right:15px;'></span>" + data.name + "</h3>";
        html += "<hr/>"
        for (var i = 0; i < data.objs.length; i++) {
            html += "<ul><li>";
            html += "<h4><b>Nombre del elemento: </b>" + data.objs[i].name + "</h4>";
            html += "<ul>";
            for (var j = 0; j < data.objs[i].attribs.length; j++) {
                html += "<li><h5><i>Atributo: </i>" + data.objs[i].attribs[j].name + "</li></h5>";
            }
            html += "</ul> <hr>";
            html += "</li></ul>";
        }
        html += "</div>";
        $("#editListSelected").html(html);

    }
    
    $('#kbSaveE').on('click', function (e) {
        if (tmpKB.name !== "" && tmpKB.name !== null) {
            
            console.log(tmpKB.name);

            var params = {
                "edit": JSON.stringify(tmpKB)
            };
            $.ajax({
                data: params,
                url: "services.php",
                context: document.body,
                type: "post",
                success: function (response) {
                    setSelectKB();
                    changePage("#selectPage");
                }
            });
        } else {
            alert("Se necesita crear una base de conocimiento.");
        }
    });


    // Elimina de a un atributo a la vez, si el elemento ya no tiene atributos, este se elimina.

    $('#kbDelE').on('click', function (e) {

            if(tmpKB.objs.length > 0)
            {
                if(tmpKB.objs[tmpKB.objs.length - 1].attribs.length > 0)
                {
                    tmpKB.objs[tmpKB.objs.length - 1].attribs.pop();
                    $("#show-ObjCreateE").hide();
                    $("#show-AttrCreateE").show();
                }
                else
                {
                   tmpKB.objs.pop();  

                    $("#show-ObjCreateE").show();
                    $("#show-AttrCreateE").hide();
                }

                editKB();
            }

    });


    $('#kbAddObjectE').on('click', function (e) {
        if (tmpKB.name !== "" && tmpKB.name !== null) {
            $("#show-AttrCreateE").hide();
            $("#show-ObjCreateE").show();
        } else {
            alert("Se necesita crear una base de conocimiento.");
        }
    });


    $('#kbAddAttribE').on('click', function (e) {
        console.log("Click");
        if (tmpKB.objs.length > 0) {
            $("#show-AttrCreateE").show();
            $("#show-ObjCreateE").hide();
        } else {
            alert("Se necesita crear un objeto primero.");
        }
    });


    $('#kbCancelE').on('click', function (e) {
        tmpKB = null;
        changePage("#selectPage");
    });


    $('#btnObjCreateE').on('click', function (e) {

         var objName = document.getElementById("objCreateE").value;

        if (objName !== "" && objName !== null) { 
            $("#show-AttrCreateE").show();
            $("#show-ObjCreateE").hide();
            var kobj = new KObject(objName);
            tmpKB.objs.push(kobj);

            editKB();

            $('html, body').scrollTop( $(document).height() );

        }
        document.getElementById("objCreateE").value = "";

        return false;

    });

    $('#btnAttrCreateE').on('click', function (e) {

        var attribName = document.getElementById("attrCreateE").value;
        if (attribName !== "" && attribName !== null) { 
            var kobj = tmpKB.objs[tmpKB.objs.length - 1];
            kobj.attribs.push(new KAttrib(attribName));
            
            editKB();

            $('html, body').scrollTop( $(document).height() );
        }

        document.getElementById("attrCreateE").value = "";

        return false;
    });



    /*************************************/
    
    
    /*************************************/

    $('#kbSelectUL').on('click', ".kbSelectLI", function (e) {
        $.ajax({
            data: {
                "load": $(e.target).html()
            },
            url: "services.php",
            context: document.body,
            type: "post",
            success: function (response) {
                selectedKB = JSON.parse(response);
                changePage("#optionPage");

            }
        });
    });
    $('#consultar').on('click', function (e) {
        $.ajax({
            data: {
                "load": $(e.target).html()
            },
            url: "services.php",
            context: document.body,
            type: "post",
            success: function (response) {
                changePage("#questionPage");
                start();
            }
        });
    });
    
    $('#listar').on('click', function (e) {
        $.ajax({
            data: {
                "load": $(e.target).html()
            },
            url: "services.php",
            context: document.body,
            type: "post",
            success: function (response) {
                changePage("#listPage");
                listKB();

            }
        });
    });
    
    

    function listKB() {
        var html = "";
        var data = selectedKB;
        html = "<div style='margin-left:20px;'><h3><span class='glyphicon glyphicon-list-alt'  aria-hidden='true' style='margin-right:15px;'></span>" + data.name + "</h3>";
        html += "<hr/>"
        for (var i = 0; i < data.objs.length; i++) {
            html += "<ul><li>";
            html += "<h4><b>Nombre del elemento: </b>" + data.objs[i].name + "</h4>";
            html += "<ul>";
            for (var j = 0; j < data.objs[i].attribs.length; j++) {
                html += "<li><h5><i>Atributo: </i>" + data.objs[i].attribs[j].name + "</li></h5>";
            }
            html += "</ul> <hr>";
            html += "</li></ul>";
        }
        html += "</div>";
        $("#listPage").html(html);

    }

    $('#kbDeleteUL').on('click', ".kbDeleteLI", function (e) {
        var params = {
            "delete": $(e.target).html()
        };
        $.ajax({
            data: params,
            url: "services.php",
            context: document.body,
            type: "post",
            success: function (response) {
                alert(response);
                setDeleteKB();
            }
        });
    });

    $('#navKB').on('click', function (e) {
        setSelectKB();
    });

    $('#kbAddObject').on('click', function (e) {
        if (tmpKB.name !== "" && tmpKB.name !== null) {
            $("#show-AttrCreate").hide();
            $("#show-KbCreate").hide();
            $("#show-ObjCreate").show();
        } else {
            alert("Se necesita crear una base de conocimiento.");
        }
    });

    $('#btnObjCreate').on('click', function (e) {
        var objName = document.getElementById("objCreate").value;

        if (objName !== "" && objName !== null) { //se agrego que sea distinto de nulo ya que el prompt si se da cancelar devuelve null no "".
            $("#show-AttrCreate").show();
            $("#show-ObjCreate").hide();
            var kobj = new KObject(objName);
            tmpKB.objs.push(kobj);
            printKB();
        }
        document.getElementById("objCreate").value = "";

        return false;
    });

    $('#kbAddAttrib').on('click', function (e) {
        console.log("Click");
        if (tmpKB.objs.length > 0) {
            $("#show-AttrCreate").show();
            $("#show-KbCreate").hide();
            $("#show-ObjCreate").hide();
        } else {
            alert("Se necesita crear un objeto primero.");
        }
    });

    $('#btnAttrCreate').on('click', function (e) {

        var attribName = document.getElementById("attrCreate").value;
        if (attribName !== "" && attribName !== null) { //se agrego que sea distinto de nulo ya que el prompt si se da cancelar devuelve null no "".
            var kobj = tmpKB.objs[tmpKB.objs.length - 1];
            kobj.attribs.push(new KAttrib(attribName));
            printKB();
        }

        document.getElementById("attrCreate").value = "";

        return false;
    });

    $('#kbSave').on('click', function (e) {
        if (tmpKB.name !== "" && tmpKB.name !== null) {

            var params = {
                "save": JSON.stringify(tmpKB)
            };
            $.ajax({
                data: params,
                url: "services.php",
                context: document.body,
                type: "post",
                success: function (response) {
                    setSelectKB();
                    changePage("#selectPage");
                }
            });
        } else {
            alert("Se necesita crear una base de conocimiento.");
        }
    });
    
    
    $('#kbCancel').on('click', function (e) {
        tmpKB = null;
        $("#kbDesc").html("");
        changePage("#selectPage");
    });


    function changePage(page) {
        $('.page.active').removeClass('active');
        $(page).addClass('active');
    }

    function printKB(erase) {
        if (erase) {
            $("#kbDesc").html("");
        } else if (tmpKB !== null) {
            var html = "Nombre: " + tmpKB.name + "<br/>";
            $.each(tmpKB.objs, function (k, v) {
                html += ".." + v.name + "<br/>";
                $.each(v.attribs, function (k, v) {
                    html += "...." + v.name + "<br/>";
                });
            });
            $("#kbDesc").html(html);
        }
    }


    function setSelectKB() {
        getKBList("#kbSelectUL", "kbSelectLI");
    }



    function setDeleteKB() {
        getKBList("#kbDeleteUL", "kbDeleteLI");
    }

    function getKBList(ul, aClass, callback) {
        $.getJSON("services.php", function (data) {
            var items = [];
            $.each(data, function (key, val) {
                items.push("<li style='color:#337ab7; font-size:16px;'><span class='glyphicon glyphicon-folder-open'  aria-hidden='true' style='margin-right:15px;'></span><a style='text-decoration:none;font-size:16px;' class='" + aClass + "' href='#' >" + val + "</a></li>");
            });
            var html = items.join("");
            $(ul).html(html);
            if (callback)
                callback();
        });

    }

    setSelectKB();

    $("#s").on('click', function (e) {
        Next('s');
    });

    $("#n").on('click', function (e) {
        Next('n');
    });

    $("#p").on('click', function (e) {
        razonando();
    });

    $("#r").on('click', function (e) {
        start();
    });

    function append(html) {
        html = $("#questionKB").html() + html;
        $("#questionKB").html(html);
    }



    function start() {
        $("#questionKB").html("");
        currentAttrib = -1;
        currentObj = -1;
        siAttrib = [];
        noAttrib = [];
        pregunta1();
    }

    function Next(resp) {
        if (nextF !== null)
            nextF(resp);
    }

    function pregunta1() {
        currentObj++;
        currentAttrib = -1;
        if (currentObj < selectedKB.objs.length) {
            intenta1();
            return;
        }
        var html = "";
        html += "<br/>No se han encontrado mas objetos.<br/>";
        append(html);
        nextF = null;
    }

    function pregunta2() {
        var obj = selectedKB.objs[currentObj];
        var html = obj.name + " concuerda con la actual descripcion.<br/>¿Sigo? ";
        append(html);
        nextF = pregunta3;
    }

    function pregunta3(resp) {
        if (resp === "s") {
            var html = " Si.<br/>";;
            append(html);
            pregunta1();
        } else {
            var html = "No.<br/>Hemos terminado<br/>";
            append(html);
            nextF = null;
        }
    }

    function intenta1() {
        var obj = selectedKB.objs[currentObj];
        if (currentAttrib < obj.attribs.length) {
            if (!sigueno(obj) || !siguesi(obj)) {
                pregunta1();
                return;
            }
            intenta2();
        } else
            pregunta1();
    }

    function intenta2() {
        var obj = selectedKB.objs[currentObj];
        currentAttrib++;
        if (currentAttrib < obj.attribs.length) {
            var att = obj.attribs[currentAttrib];
            if (preg(att)) {
                var html = "<br/>Es / Hace / Tiene " + att.name + " ?";
                html = $("#questionKB").html() + html;
                $("#questionKB").html(html);
                nextF = intenta3;
            }
        } else
            pregunta2();
    }

    function intenta3(resp) {
        var obj = selectedKB.objs[currentObj];
        var att = obj.attribs[currentAttrib];

        switch (resp) {
            case 's':
                var html = " Si.<br/>";
                append(html);
                siAttrib.push(att);
                intenta2();
                break;
            case 'n':
                var html = " No.<br/>";
                append(html);
                noAttrib.push(att);
                pregunta1();
                break;
        }
    }

    function sigueno(obj) {
        var r = true;
        $.each(obj.attribs, function (k1, v1) {
            $.each(noAttrib, function (k2, v2) {
                if (v1.name === v2.name) {
                    r = false;
                    return false;
                }
            });
        });
        return true;
    }

    function siguesi(obj) {
        var r = true;
        $.each(siAttrib, function (k1, v1) {
            var ok = false;
            $.each(obj.attribs, function (k2, v2) {
                if (v1.name === v2.name)
                    ok = true;
            });
            if (ok === false) {
                r = false;
                return false;
            }
        });
        return r;
    }

    function preg(attrib) {
        var r = true;
        $.each(siAttrib, function (k, v) {
            if (v.name === attrib.name) {
                r = false;
                return false;
            }
        });
        return r;
    }

    function razonando() {
        var obj = selectedKB.objs[currentObj];
        var html = "<br/>Intentando " + obj.name + "<br/>";
        if (siAttrib.length > 0)
            html += "<br/>Es / Tiene / Hace:<br/>";
        $.each(siAttrib, function (k, v) {
            html += v.name + "<br/>";
        });
        if (noAttrib.length > 0)
            html += "<br/>No es/tiene/hace:<br/>";
        $.each(noAttrib, function (k, v) {
            html += v.name + "<br/>";
        });
        html += "<br/>";
        append(html);
    }

    var nextF = pregunta1;

    var tmpKB = null;

    var selectedKB = null;

    var currentObj = 0;

    var currentAttrib = 0;

    var siAttrib = [];

    var noAttrib = [];
});
