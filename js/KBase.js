function KBase(name){
    this.name = name;
    this.objs = [];
}
function KObject(name){
    this.name = name;
    this.attribs = [];
}

function KAttrib(name){
    this.name = name;
}

function KRejectedObject(name, attrib, condition){
    this.name = name;
    this.attrib = attrib;
    this.condition = condition;
}