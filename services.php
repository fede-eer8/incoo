<?php

if (isset($_POST['save'])) {
    $json = $_POST['save'];
    $obj = json_decode($json);
    $file = "json/" . $obj->name . ".json";
    file_put_contents($file, $json);
} 

else if (isset($_POST['load'])) {
    $jsonName = $_POST['load'];
    $jsonPath = 'json/' . $jsonName . '.json';
    if (file_exists($jsonPath)) {
        echo file_get_contents($jsonPath);
    } else {
        echo '{"error":true}';
    }
}



else if (isset($_POST['edit'])) {

    $json = $_POST['edit'];
    $obj = json_decode($json);
    $file = "json/" . $obj->name . ".json";
    
    if (file_exists($file)) {

        $ext = [];
        
        $files = scandir('json');
        foreach ($files as $k => $f) {
            if ($f !== '.' && $f !== '..') {
                $f = explode('.', $f);

                if($f[0] == $obj->name && $f[1] != "json")
                {
                    array_push($ext, $f[1]);
                }
            }
        }
        
        if(count($ext) > 0)
        {
            $ext = asort($ext);
            
            $num = count($ext) + 1 ;
            
            $num = str_pad($num, 3, "0", STR_PAD_LEFT);
            
            rename($file, "json/" . $obj->name . "." . $num);
        }
        else
        {
            rename($file, "json/" . $obj->name . ".001");
        }

    } 

    file_put_contents($file, $json);


}

else if (isset($_POST['delete'])) {
    $file = $_POST['delete'];
    if (unlink("json/".$file.".json")) {
        echo $file . " ha sido eliminado.";
    }else{
        echo "ha ocurrido un error al intentar borrar $file.";
    }
}  else {
    $kbb = scandir('json');
    $fkbb = [];
    foreach ($kbb as $k => $v) {
        if ($v !== '.' && $v !== '..') {
            $v = explode('.', $v);
            
            // Si la estensión del archivo es "json" la envío
            if($v[1] == "json")
            {
                array_pop($v);
                $v = implode('.', $v);
                array_push($fkbb, $v);
            }
        }
    }
    echo json_encode($fkbb);
}
